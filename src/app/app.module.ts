import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { NavbarComponent } from './dashboard/navbar/navbar.component';
import { ListadoEmpleadosComponent } from './dashboard/listado-empleados/listado-empleados.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddEditEmpleadoComponent } from './dashboard/add-edit-empleado/add-edit-empleado.component';
import { MensajeConfirmacionComponent } from './dashboard/shared/mensaje-confirmacion/mensaje-confirmacion.component';
import { AngularMaterialModule } from './dashboard/shared/angular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    ListadoEmpleadosComponent,
    AddEditEmpleadoComponent,
    MensajeConfirmacionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
