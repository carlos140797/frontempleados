export class Empleado{
    id!:number;
    firstName!: string;
    middleName!: string;
    lastName!: string;
    secondLastName!: string;
    country!: string;
    documentType!: string;
    document!: string;
    area!: string;
    email!: string;
    state!: string;
    addmissionDate!: Date;
    createdAt!: Date;
    updateAt?: Date;
}