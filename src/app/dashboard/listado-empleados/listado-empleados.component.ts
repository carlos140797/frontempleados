import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EmpleadosService } from "../../services/empleados.service";
import { MensajeConfirmacionComponent } from '../shared/mensaje-confirmacion/mensaje-confirmacion.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-listado-empleados',
  templateUrl: './listado-empleados.component.html',
  styleUrls: ['./listado-empleados.component.css']
})

/**
 * Generacion de lista inicial con los componentes de acciones
 */
export class ListadoEmpleadosComponent {
  displayedColumns: string[] = ['documentType', 'document', 'firstName', 'lastName', 'email', 'country', 'area', 'addmissionDate', 'createdAt', 'updateAt', 'actions'];
  listEmpleados: any[] = [];
  dataSource = new MatTableDataSource(this.listEmpleados);
  
  constructor(private empleadosService: EmpleadosService, public dialog: MatDialog,
    public snackBar: MatSnackBar) {
      this.getEmpleados();
    }

  /**
   * recupera todos los
   */
  getEmpleados(): void {    
      this.empleadosService.readAll().subscribe(data =>{
      this.listEmpleados = data;      
      this.dataSource = new MatTableDataSource(this.listEmpleados);
    })
  }

  /**
   * Eliminacion del empleado
   * @param index id con el que se eliminara el empleado
   */
  eliminarEmpleado(index: number){
    const dialogRef = this.dialog.open(MensajeConfirmacionComponent, {
      width: '350px',
      data: {mensaje: 'Esta seguro que desea eliminar el Empleado?'}
          });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'aceptar') { 
        this.empleadosService.delete(index).subscribe((data)=>{
          if (data) {
            this.snackBar.open('El empleado fue eliminado con exito!', '', {
              duration: 3000
            });
            this.getEmpleados();
          } else {
            this.snackBar.open('El empleado No Existe!', '', {
              duration: 3000
            });
            this.getEmpleados();
          }
          
        });      
      }
      
    });
    
  }

  /**
   * Aplicacion del filtro para facilitar la busqueda de informacion
   * @param event 
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
