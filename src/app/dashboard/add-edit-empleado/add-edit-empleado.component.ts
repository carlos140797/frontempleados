import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Empleado } from 'src/app/models/empleado';
import { EmpleadosService } from 'src/app/services/empleados.service';


@Component({
  selector: 'app-add-edit-empleado',
  templateUrl: './add-edit-empleado.component.html',
  styleUrls: ['./add-edit-empleado.component.css'],
  
})

/**
 * Clase de agregar y eliminar empleados
 */
export class AddEditEmpleadoComponent  implements OnInit{
  tiposDocumentos: any[] = ['Cédula de Ciudadanía', 'Cédula de Extranjería', 'Pasaporte', 'Permiso Especial'];
  paises: any[] = ['Colombia',  'Estados Unidos'];
  areas: any[] = ['Administración', 'Financiera', 'Compras', 'Infraestructura', 'Operación', 'Talento Humano', 'Servicios Varios'];
  accion = 'Crear';
  id: any;
  empleado: Empleado;

  myForm: FormGroup;
  maxDate: Date;
  /**
   * Metodo constructor
   * @param fb constructor del formulario
   * @param empleadoService para acceder al empleado y almacenarlos y recuperarlos sin perdidas de informacion
   * @param route redireccionar a paginas
   * @param snackBar desplegar los cuadros modales
   * @param aRoute recuperar de parametros a otras pag
   */
  constructor(private fb: FormBuilder, private empleadoService: EmpleadosService, private route: Router, private snackBar: MatSnackBar, private aRoute: ActivatedRoute) { 
    this.empleado = new Empleado;
    this.maxDate = new Date();    
    this.myForm = this.fb.group({
      firstName: ['',[Validators.required,Validators.maxLength(20),Validators.pattern('[a-zA-Z ]*')]],
      middleName: ['',[Validators.pattern('[a-zA-Z ]*')]],
      lastName: ['',[Validators.required,Validators.maxLength(20),Validators.pattern('[a-zA-Z ]*')]],
      secondLastName: ['',[Validators.required,Validators.maxLength(20),Validators.pattern('[a-zA-Z ]*')]],
      country: [''],
      documentType: [''],
      document: ['',[Validators.required,Validators.pattern('[0-9 ]*')]],
      area: [''],
      email: [{value: '', disabled: true}],
      createdAt: [{value: '', disabled: true}],
      updateAt: [{value: '', disabled: true}],
      state: [{value: '', disabled: true}],
      addmissionDate: ['',[Validators.required]]
    })
    const idParam = 'id';
    this.id = this.aRoute.snapshot.params[idParam];
  }

  
  /**
   * Almacenando la informacion del empleado para enviarlo sea a editar o agregar
   */
  saveEmployee(){
    const empleado: Empleado = {
      firstName: this.myForm.get("firstName")?.value,
      middleName: this.myForm.get("middleName")?.value,
      lastName: this.myForm.get("lastName")?.value,
      secondLastName: this.myForm.get("secondLastName")?.value,
      country: this.myForm.get("country")?.value,
      documentType: this.myForm.get("documentType")?.value,
      document: this.myForm.get("document")?.value,
      area: this.myForm.get("area")?.value,
      email: this.myForm.get("email")?.value,
      state: this.myForm.get("state")?.value,
      addmissionDate: this.myForm.get("addmissionDate")?.value,
      createdAt: this.myForm.get("createdAt")?.value,
      updateAt: this.myForm.get("updateAt")?.value,
      id: this.id
    };
    if (this.id  !== undefined) {
      
      this.editarEmpleado(empleado);
    } else {
      this.agregarEmpleado(empleado);
    }
    
  }

  /**
   * Añadir el empleado y nos arroja el mensaje de aprobacion
   * @param empleado 
   */
  agregarEmpleado(empleado: Empleado) {
    this.empleadoService.create(empleado).subscribe((data)=>{
      if (data) {
        this.snackBar.open('El empleado fue registrado con exito!', '', {
          duration: 3000
        });
        
        this.route.navigate(['/']);
      }else{
        this.snackBar.open('Revisa el documento', '', {
          duration: 3000
        });
      }
      
    });
    
  }

  /**
   * Edicion de empleado, con validaciones
   * @param empleado 
   */
  editarEmpleado(empleado: Empleado) {
    this.empleadoService.update(this.id, empleado).subscribe((data)=>{
      if (data) {
        this.snackBar.open('El empleado fue actualizado con exito!', '', {
          duration: 3000
        });
        this.route.navigate(['/']);
      } else {        
        this.snackBar.open('Revisa el documento', '', {
          duration: 3000
        });
      }
    })
    
  }
  /**
   * Recupera la clase de accion y si recupera la informacion inicial
   */
  ngOnInit(): void {
    if (this.id !== undefined) {
      this.accion = 'Editar';
      this.esEditar();
    }
  }

  /**
   * recupera los datos para plasmarlos en la interfaz
   */
  esEditar() {
    this.empleadoService.read(this.id).subscribe(data =>{
      this.empleado = data;
    this.myForm.patchValue({
      firstName: this.empleado.firstName,
      middleName: this.empleado.middleName,
      lastName: this.empleado.lastName,
      secondLastName: this.empleado.secondLastName,
      country: this.empleado.country,
      documentType: this.empleado.documentType,
      document: this.empleado.document,
      area: this.empleado.area,
      email: this.empleado.email,
      state: this.empleado.state,
      addmissionDate: this.empleado.addmissionDate,
      createdAt: this.empleado.createdAt,
      updateAt: this.empleado.updateAt,
    })
  })
  }
}