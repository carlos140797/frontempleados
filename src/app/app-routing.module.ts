import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditEmpleadoComponent } from './dashboard/add-edit-empleado/add-edit-empleado.component';
import { ListadoEmpleadosComponent } from './dashboard/listado-empleados/listado-empleados.component';

const routes: Routes = [
  {path: '', component: ListadoEmpleadosComponent},
  {path: 'add', component: AddEditEmpleadoComponent},
  {path: 'edit/:id', component: AddEditEmpleadoComponent},
  {path: '**', component: ListadoEmpleadosComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
